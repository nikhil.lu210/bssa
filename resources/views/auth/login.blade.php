<!doctype html>
<html class="fixed">
<head>
    <!-- Basic -->
    <meta charset="UTF-8">

    {{-- CSRF Token--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--  Page Title  --}}
    <title> BSSA | LOGIN</title>
    <link rel="shortcut icon" href="{{ asset('frontend/images/icon.png') }}" type="image/x-icon">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('backend/octopus/vendor/font-awesome/css/font-awesome.css') }}" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('backend/octopus/css/theme.css') }}" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('backend/octopus/css/skins/default.css') }}" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset('backend/octopus/css/theme-custom.css') }}" />

    <!-- Custom CSS -->
    {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('backend/css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('backend/css/responsive.css') }}" />

    <!-- Head Libs -->
    <script src="{{ asset('backend/octopus/vendor/modernizr/modernizr.js') }}" /></script>

</head>
<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo pull-left">
					<img src="{{ asset('backend/images/logo.png') }}" height="54"  alt="LOGO" />
				</a>

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign In</h2>
					</div>
					<div class="panel-body">
						<form action="{{ route('loginWithUser') }}" method="post">
                        @csrf
							<div class="form-group mb-lg">
								<label>Email or Username</label>
								<div class="input-group input-group-icon">
									<input name="email" type="text" class="form-control input-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
                                    </span>
								</div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">Password</label>
									{{-- <a href="pages-recover-password.html" class="pull-right">Lost Password?</a> --}}
								</div>
								<div class="input-group input-group-icon">
									<input name="password" type="password" class="form-control input-lg {{ $errors->has('password') ? ' is-invalid' : '' }}" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
                                    </span>
								</div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
							</div>

							<div class="row">
								<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
										<input id="RememberMe" name="rememberme" type="checkbox"/>
										<label for="RememberMe">Remember Me</label>
									</div>
								</div>
								<div class="col-sm-4 text-right">
									<button type="submit" class="btn btn-primary hidden-xs">Sign In</button>
									<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
								</div>
							</div>

							{{-- <span class="mt-lg mb-lg line-thru text-center text-uppercase">
								<span>or</span>
							</span>

							<div class="mb-xs text-center">
								<a class="btn btn-facebook mb-md ml-xs mr-xs">Connect with <i class="fa fa-facebook"></i></a>
								<a class="btn btn-twitter mb-md ml-xs mr-xs">Connect with <i class="fa fa-twitter"></i></a>
							</div>

							<p class="text-center">Don't have an account yet? <a href="pages-signup.html">Sign Up!</a> --}}

						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md">&copy; Copyright 2019. All rights reserved. Template by <a href="https://veechitechnologies.com">Veechi Technologies</a>.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
        <script src="{{ asset('backend/octopus/vendor/jquery/jquery.js') }}"></script>
        <script src="{{ asset('backend/octopus/vendor/bootstrap/js/bootstrap.js') }}"></script>
        <script src="{{ asset('backend/octopus/vendor/nanoscroller/nanoscroller.js') }}"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="{{ asset('backend/octopus/js/theme.js') }}"></script>

        <!-- Theme Custom -->
        <script src="{{ asset('backend/octopus/js/theme.custom.js') }}"></script>

        <!-- Theme Initialization Files -->
        <script src="{{ asset('backend/octopus/js/theme.init.js') }}"></script>

        <!-- custom js -->
        <script src="{{ asset('backend/js/script.js') }}"></script>
        <script src="{{ asset('backend/js/responsive.js') }}"></script>

	</body>
</html>
