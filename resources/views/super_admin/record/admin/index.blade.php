@extends('layouts.super_admin.app')

@section('page_title', '| Admin')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.DTTT.btn-group {
    position: absolute;
    top: -75px;
    right: 0;
}
</style>
@endsection


@section('content')
<header class="page-header">
    <h2><b>All Admins</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('superadmin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Admins</span></li>
            <li><span>All Admins</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">All Admins</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Admin Name</th>
                    <th>Mobile No.</th>
                    <th>Email Address</th>
                    <th>Register Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($admins as $key => $admin)
                    <tr class="gradeX">
                        <td> @if($key+1 < 10){{ 0 }}@endif{{ $key+1  }} </td>
                        <td>{{ $admin->name }}</td>
                        <td>{{ $admin->mobile }}</td>
                        <td>{{ $admin->email }}</td>
                        <td>{{ $admin->created_at->format('d-m-Y')}}</td>

                        <td class="action-td">
                            <a href="{{ route('superadmin.record.admin.destroy', ['id' => $admin->id]) }}" onclick="return confirm('Are You Sure Want To Delete This Admin..?')" class="btn btn-default btn-round-custom"><i class="fa fa-trash-o"></i></a>
                            <a href="{{ route('superadmin.record.admin.show', ['id' => $admin->id]) }}" class="btn btn-default btn-round-custom"><i class="fa fa-info"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>

<!-- end: page -->
@endsection


@section('scripts')
    {{--  External Javascript  --}}
    <script src="{{ asset('backend/octopus/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('backend/octopus/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('backend/octopus/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('backend/octopus/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>


    <script src="{{ asset('backend/octopus/js/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('backend/octopus/js/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('backend/octopus/js/tables/examples.datatables.tabletools.js') }}"></script>
@endsection
