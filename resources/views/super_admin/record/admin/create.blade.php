@extends('layouts.super_admin.app')

@section('page_title', '| Add New Admin')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.form-bordered .form-group{
    border-bottom: 0px solid #fff;
}

.md-editor.active{
    border-color: #fec62b;
    box-shadow: none !important;
    outline: none !important;
}
.md-editor.active textarea{
    border-top: 1px dashed #fec62b;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>Add New Admin</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Settings</span></li>
            <li><span>Add New Admin</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Add New Admin</h2>
    </header>

<form class="form-horizontal form-bordered" action="{{ route('superadmin.record.admin.store') }}" method="POST">
    @csrf
    <div class="panel-body">

        <div class="form-group">
            <label class="col-md-2 control-label" for="name">Full Name *</label>
            <div class="col-md-8">
                <input value="{{old('name')}}" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="username">Username *</label>
            <div class="col-md-8">
                <input value="{{old('username')}}" type="text" class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}" id="username" name="username">
                @if ($errors->has('username'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="email">Email *</label>
            <div class="col-md-8">
                <input value="{{old('email')}}" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email">
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="mobile">Mobile Number *</label>
            <div class="col-md-8">
                <input value="{{old('mobile')}}" type="phone" class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" id="mobile" name="mobile">
                @if ($errors->has('mobile'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('mobile') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="note">Note </label>
            <div class="col-md-8">
                <textarea class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" rows="2">{{old('note')}}</textarea>
                {{-- <small>If any note about this Admin.</small> --}}
                @if ($errors->has('note'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('note') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="password">Password *</label>
            <div class="col-md-8">
                <input value="{{old('password')}}" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password">
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

    </div>

    <div class="panel-footer text-right">
        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-warning">Register New Admin</button>
    </div>

</form>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/to-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

@endsection
