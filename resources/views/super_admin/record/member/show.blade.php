@extends('layouts.super_admin.app')

@section('page_title', '| All Members | Details')

@section('stylesheet_links')
{{-- External CSS Links --}}

@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
    .panel-actions{
        top: 13px;
    }

    .table.table-bordered tbody tr th{
        width: 30% !important;
    }
    .table.table-bordered tbody tr td{
        width: 70% !important;
    }
    ul, ol {
        padding-left: 15px;
    }

    .menu-name{
        font-weight: 600;
    }
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>Order Details</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('superadmin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Records</span></li>
            <li>
                <a href="{{ Route('superadmin.record.member.index') }}">
                    All Members
                </a>
            </li>
            <li><span>{{$member->name}}</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <button class="btn btn-default btn-sm" onclick="window.history.go(-1); return false;">Back</button>
        </div>
        <h2 class="panel-title">{{$member->name}}</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped table-hover">
            <tbody>

                <tr>
                    <th>Author Admin:</th>
                    <td><b>{{$member->user->name}}</b></td>
                </tr>

                <tr>
                    <th>Full Name :</th>
                    <td>{{$member->name}}</td>
                </tr>

                <tr>
                    <th>Email Address :</th>
                    <td>{{$member->email}}</td>
                </tr>

                <tr>
                    <th>Mobile Number :</th>
                    <td>{{$member->mobile}}</td>
                </tr>

                <tr>
                    <th>Address :</th>
                    <td>{{$member->address}}</td>
                </tr>

                <tr>
                    <th>Post Code :</th>
                    <td>{{$member->post_code}}</td>
                </tr>

                <tr>
                    <th>Birthdate :</th>
                    <td>{{$member->birthdate}}</td>
                </tr>

                <tr>
                    <th>Prev-Club :</th>
                    <td>
                        @if ($member->reg_status == 1)
                            Yes
                        @else
                            No
                        @endif
                    </td>
                </tr>

                <tr>
                    <th>Registered Club Name :</th>
                    <td>{{$member->reg_club_name}}</td>
                </tr>

                <tr>
                    <th>Legal Guardian :</th>
                    <td>{{$member->guardian_name}}</td>
                </tr>

                <tr>
                    <th>Relation With Him/Her :</th>
                    <td>{{$member->relation}}</td>
                </tr>

                <tr>
                    <th>Guardian Mobile Number :</th>
                    <td>{{$member->guardian_mobile}}</td>
                </tr>

                <tr>
                    <th>Guardian Phone Number :</th>
                    <td>{{$member->guardian_telephone}}</td>
                </tr>

                <tr>
                    <th>Guardian Email :</th>
                    <td>{{$member->guardian_email}}</td>
                </tr>

                <tr>
                    <th>Emergency Contact Name :</th>
                    <td>{{$member->emergency_name_one}}</td>
                </tr>

                <tr>
                    <th>Emergency Contact Number :</th>
                    <td>{{$member->emergency_mobile_one}}</td>
                </tr>

                <tr>
                    <th>Emergency Contact Name :</th>
                    <td>{{$member->emergency_name_two}}</td>
                </tr>

                <tr>
                    <th>Emergency Contact Number :</th>
                    <td>{{$member->emergency_mobile_two}}</td>
                </tr>

                <tr>
                    <th>Any Medical Issue's :</th>
                    <td>{{$member->medical_note_one}}</td>
                </tr>

                <tr>
                    <th>Any Meditation's :</th>
                    <td>{{$member->medical_note_two}}</td>
                </tr>

            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}

@endsection
