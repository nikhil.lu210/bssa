<!-- Basic -->
<meta charset="UTF-8">

{{-- CSRF Token--}}
<meta name="csrf-token" content="{{ csrf_token() }}">

{{--  Page Title  --}}
<title> BSSA | Admin @yield('page_title') </title>
<link rel="shortcut icon" href="{{ asset('frontend/images/icon.png') }}" type="image/x-icon">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- Web Fonts  -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

<!-- Vendor CSS -->
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap/css/bootstrap.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/font-awesome/css/font-awesome.css') }}" />


@yield('stylesheet_links')

<!-- Theme CSS -->
<link rel="stylesheet" href="{{ asset('backend/octopus/css/theme.css') }}" />

<!-- Skin CSS -->
<link rel="stylesheet" href="{{ asset('backend/octopus/css/skins/default.css') }}" />

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="{{ asset('backend/octopus/css/theme-custom.css') }}" />

<!-- Custom CSS -->
{{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
<link rel="stylesheet" href="{{ asset('backend/css/style.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/css/responsive.css') }}" />

<!-- Head Libs -->
<script src="{{ asset('backend/octopus/vendor/modernizr/modernizr.js') }}" /></script>

@yield('stylesheet')
