<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

        <div class="sidebar-header">
            <div class="sidebar-title">
                <b>Admin Dashboard</b>
            </div>
            <div class="sidebar-toggle lg hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                <i class="fa fa-arrow-left" aria-label="Toggle sidebar"></i>
            </div>
            <div class="sidebar-toggle sm hidden-xs d-none" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                <i class="fa fa-arrow-right" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <div class="nano">
            <div class="nano-content">
                <nav id="menu" class="nav-main" role="navigation">
                    <ul class="nav nav-main">

                        {{-- Dashboard --}}
                        <li class="{{ Request::is('admin') ? 'nav-active' : '' }}">
                            <a href="{{ Route('admin.dashboard') }}">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>

                        {{-- Members --}}
                        <li class="nav-parent {{ Request::is('admin/member*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span>Members</span>
                            </a>

                            <ul class="nav nav-children">
                                <li class="{{ Request::is('admin/member') ? 'nav-active' : '' }}">
                                    <a href="{{ route('admin.member.index') }}">All Members</a>
                                </li>

                                <li class="{{ Request::is('admin/member/filter*') ? 'nav-active' : '' }}">
                                    <a href="{{ route('admin.member.filter') }}">Members I Added</a>
                                </li>
                            </ul>
                        </li>

                        {{-- Settings --}}
                        <li class="nav-parent {{ Request::is('admin/setting*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fa fa-cogs" aria-hidden="true"></i>
                                <span>Settings</span>
                            </a>

                            <ul class="nav nav-children">
                                <li class="{{ Request::is('admin/setting/member/create*') ? 'nav-active' : '' }}">
                                    <a href="{{ route('admin.member.create') }}">Add New Member</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>

    </aside>
    <!-- end: sidebar -->
