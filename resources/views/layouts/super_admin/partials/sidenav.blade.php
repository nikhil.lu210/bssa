<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

        <div class="sidebar-header">
            <div class="sidebar-title">
                <b>Super Admin Dashboard</b>
            </div>
            <div class="sidebar-toggle lg hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                <i class="fa fa-arrow-left" aria-label="Toggle sidebar"></i>
            </div>
            <div class="sidebar-toggle sm hidden-xs d-none" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                <i class="fa fa-arrow-right" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <div class="nano">
            <div class="nano-content">
                <nav id="menu" class="nav-main" role="navigation">
                    <ul class="nav nav-main">

                        {{-- Dashboard --}}
                        <li class="{{ Request::is('superadmin') ? 'nav-active' : '' }}">
                            <a href="{{ Route('superadmin.dashboard') }}">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>

                        {{-- Records --}}
                        <li class="nav-parent {{ Request::is('superadmin/record*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span>Records</span>
                            </a>

                            <ul class="nav nav-children">
                                <li class="{{ Request::is('superadmin/record/admin*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('superadmin.record.admin.index') }}">All Admins</a>
                                </li>

                                <li class="{{ Request::is('superadmin/record/member*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('superadmin.record.member.index') }}">All Members</a>
                                </li>
                            </ul>
                        </li>

                        {{-- Settings --}}
                        <li class="nav-parent {{ Request::is('superadmin/setting*') ? 'nav-active nav-expanded' : '' }}">
                            <a>
                                <i class="fa fa-cogs" aria-hidden="true"></i>
                                <span>Settings</span>
                            </a>

                            <ul class="nav nav-children">
                                <li class="{{ Request::is('superadmin/setting/admin/create*') ? 'nav-active' : '' }}">
                                    <a href="{{ Route('superadmin.record.admin.create') }}">Add New Admin</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>

    </aside>
    <!-- end: sidebar -->
