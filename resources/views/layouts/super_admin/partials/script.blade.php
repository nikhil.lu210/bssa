<!-- Vendor -->
<script src="{{ asset('backend/octopus/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/nanoscroller/nanoscroller.js') }}"></script>

@yield('scripts')

<!-- Theme Base, Components and Settings -->
<script src="{{ asset('backend/octopus/js/theme.js') }}"></script>

<!-- Theme Custom -->
<script src="{{ asset('backend/octopus/js/theme.custom.js') }}"></script>

<!-- Theme Initialization Files -->
<script src="{{ asset('backend/octopus/js/theme.init.js') }}"></script>

<!-- custom js -->
<script src="{{ asset('backend/js/script.js') }}"></script>
<script src="{{ asset('backend/js/responsive.js') }}"></script>
