<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="fixed">
<head>
    @include('layouts.super_admin.partials.header')

    {{--  This will include all CSS files which are connected into header.blade.php in partials Folder  --}}

</head>
<body class="wrap-content">

    <section class="body" id="app_vue">
        @include('layouts.super_admin.partials.topnav')

        <div class="inner-wrapper">
            @include('layouts.super_admin.partials.sidenav')

            <section role="main" class="content-body">
                @yield('content')
            </section>

        </div>

    </section>

    @include('layouts.super_admin.partials.script')

    {{--  This will include all JS files which are connected into javascript.blade.php in partials Folder  --}}
</body>
</html>
