@extends('layouts.admin.app')

@section('page_title', '| Dashboard')

@section('stylesheet')
    {{--  External CSS  --}}

@endsection

@section('content')
<header class="page-header">
    <h2><b>Dashboard</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Dashboard</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="dashboard-part">
    <div class="row">
        <div class="col-md-6 col-lg-12 col-xl-6">
            <div class="row">

                <div class="col-md-6">
                    <div class="panel panel-featured-left panel-featured-primary">
                        <div class="panel-body">
                            <div class="widget-summary widget-summary-sm">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-users"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><b>Total Members</b></h4>
                                        <div class="info">
                                            <strong class="amount">{{$totalMembers}}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-featured-left panel-featured-primary">
                        <div class="panel-body">
                            <div class="widget-summary widget-summary-sm">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-male"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><b>Members I Added</b></h4>
                                        <div class="info">
                                            <strong class="amount">{{$filterMembers}}</strong>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
    {{--  External Javascript  --}}

@endsection
