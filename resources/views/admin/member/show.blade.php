@extends('layouts.admin.app')

@section('page_title', '| Member Details')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.form-bordered .form-group{
    border-bottom: 0px solid #fff;
}

.md-editor.active{
    border-color: #fec62b;
    box-shadow: none !important;
    outline: none !important;
}
.md-editor.active textarea{
    border-top: 1px dashed #fec62b;
}
</style>
@endsection

@section('content')
<header class="page-header">
    <h2><b>{{$member->name}}'s Details</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('admin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Members</span></li>
            <li><span>All Members</span></li>
            <li><span>{{$member->name}}</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">{{$member->name}}</h2>
    </header>

<form class="form-horizontal form-bordered" action="{{ route('admin.member.update', ['id' => $member->id]) }}" method="POST">
    @csrf
    <div class="panel-body">

        <div class="form-group">
            <label class="col-md-2 control-label" for="name">Full Name</label>
            <div class="col-md-8">
                <input value="{{$member->name}}" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name">
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="email">Email Address</label>
            <div class="col-md-8">
                <input value="{{$member->email}}" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email">
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="mobile">Mobile Number</label>
            <div class="col-md-8">
                <input value="{{$member->mobile}}" type="text" class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" id="mobile" name="mobile">
                @if ($errors->has('mobile'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('mobile') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="address">Address</label>
            <div class="col-md-8">
                <textarea class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" rows="3">{{$member->address}}</textarea>
                @if ($errors->has('address'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="post_code">Postal Code</label>
            <div class="col-md-8">
                <input value="{{$member->post_code}}" type="text" class="form-control {{ $errors->has('post_code') ? ' is-invalid' : '' }}" id="post_code" name="post_code">
                @if ($errors->has('post_code'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('post_code') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="birthdate">Date Of Birth</label>
            <div class="col-md-8">
                <input value="{{$member->birthdate}}" type="text" data-plugin-datepicker class="form-control {{ $errors->has('birthdate') ? ' is-invalid' : '' }}" id="birthdate" name="birthdate">
                @if ($errors->has('birthdate'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('birthdate') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Are you already registered with a club?</label>
            <div class="col-sm-8">
                <div class="radio-custom radio-primary">
                    <input class="{{ $errors->has('reg_status') ? ' is-invalid' : '' }}" id="yes" name="reg_status" type="radio" value="1" @if ($member->reg_status == 1) checked @endif>
                    <label for="yes">Yes</label>
                </div>
                <div class="radio-custom radio-primary">
                    <input class="{{ $errors->has('reg_status') ? ' is-invalid' : '' }}" id="no" name="reg_status" type="radio" value="0" @if ($member->reg_status == 0) checked @endif>
                    <label for="no">No</label>
                </div>
                @if ($errors->has('reg_status'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('reg_status') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="reg_club_name">If yes, Which Club?</label>
            <div class="col-md-8">
                <input value="{{$member->reg_club_name}}" type="text" class="form-control {{ $errors->has('reg_club_name') ? ' is-invalid' : '' }}" id="reg_club_name" name="reg_club_name">
                @if ($errors->has('reg_club_name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('reg_club_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <h2>Parents / Guardian Details</h2>

        <div class="form-group">
            <label class="col-md-2 control-label" for="guardian_name">Parent / Guardian Name</label>
            <div class="col-md-8">
                <input value="{{$member->guardian_name}}" type="text" class="form-control {{ $errors->has('guardian_name') ? ' is-invalid' : '' }}" id="guardian_name" name="guardian_name">
                @if ($errors->has('guardian_name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('guardian_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="relation">Relation to the above player</label>
            <div class="col-md-8">
                <input value="{{$member->relation}}" type="text" class="form-control {{ $errors->has('relation') ? ' is-invalid' : '' }}" id="relation" name="relation">
                @if ($errors->has('relation'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('relation') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="guardian_mobile">Guardian Mobile No.</label>
            <div class="col-md-8">
                <input value="{{$member->guardian_mobile}}" type="text" class="form-control {{ $errors->has('guardian_mobile') ? ' is-invalid' : '' }}" id="guardian_mobile" name="guardian_mobile">
                @if ($errors->has('guardian_mobile'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('guardian_mobile') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="guardian_telephone">Guardian Telephone No.</label>
            <div class="col-md-8">
                <input value="{{$member->guardian_telephone}}" type="text" class="form-control {{ $errors->has('guardian_telephone') ? ' is-invalid' : '' }}" id="guardian_telephone" name="guardian_telephone">
                @if ($errors->has('guardian_telephone'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('guardian_telephone') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="guardian_email">Guardian Email</label>
            <div class="col-md-8">
                <input value="{{$member->guardian_email}}" type="email" class="form-control {{ $errors->has('guardian_email') ? ' is-invalid' : '' }}" id="guardian_email" name="guardian_email">
                @if ($errors->has('guardian_email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('guardian_email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <h2>Emergency Contact Details <sup>(Please provide two emergency contacts)</sup></h2>

        <div class="form-group">
            <label class="col-md-2 control-label" for="emergency_name_one">Full Name</label>
            <div class="col-md-8">
                <input value="{{$member->emergency_name_one}}" type="text" class="form-control {{ $errors->has('emergency_name_one') ? ' is-invalid' : '' }}" id="emergency_name_one" name="emergency_name_one">
                @if ($errors->has('emergency_name_one'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('emergency_name_one') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="emergency_mobile_one">Mobile Number</label>
            <div class="col-md-8">
                <input value="{{$member->emergency_mobile_one}}" type="text" class="form-control {{ $errors->has('emergency_mobile_one') ? ' is-invalid' : '' }}" id="emergency_mobile_one" name="emergency_mobile_one">
                @if ($errors->has('emergency_mobile_one'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('emergency_mobile_one') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="emergency_name_two">Full Name</label>
            <div class="col-md-8">
                <input value="{{$member->emergency_name_two}}" type="text" class="form-control {{ $errors->has('emergency_name_two') ? ' is-invalid' : '' }}" id="emergency_name_two" name="emergency_name_two">
                @if ($errors->has('emergency_name_two'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('emergency_name_two') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="emergency_mobile_two">Mobile Number</label>
            <div class="col-md-8">
                <input value="{{$member->emergency_name_two}}" type="text" class="form-control {{ $errors->has('emergency_mobile_two') ? ' is-invalid' : '' }}" id="emergency_mobile_two" name="emergency_mobile_two">
                @if ($errors->has('emergency_mobile_two'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('emergency_mobile_two') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <h2>Medical Details</h2>

        <div class="form-group">
            <label class="col-md-2 control-label" for="medical_note_one">Any Medical Issues? </label>
            <div class="col-md-8">
                <textarea class="form-control {{ $errors->has('medical_note_one') ? ' is-invalid' : '' }}" name="medical_note_one" rows="2">{{$member->medical_note_one}}</textarea>
                <small>Please indicate if there are any medical conditions that we need to be aware of. (eg: Asthma / Alergy)</small>
                @if ($errors->has('medical_note_one'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('medical_note_one') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="medical_note_two">Any medication required?</label>
            <div class="col-md-8">
                <textarea class="form-control {{ $errors->has('medical_note_two') ? ' is-invalid' : '' }}" name="medical_note_two" rows="3">{{$member->medical_note_two}}</textarea>
                <small>Any medication required should be given to the coach in charge, clearly marked (in its prescriptions container if applicable) with name and full instructions for use. Please provide additional information if necessary.</small>
                @if ($errors->has('medical_note_two'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('medical_note_two') }}</strong>
                    </span>
                @endif
            </div>
        </div>

    </div>
    @if ($member->admin_id == Auth::user()->id)
        <div class="panel-footer text-right">
            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-warning">Update information</button>
        </div>
    @endif

</form>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/to-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

@endsection
