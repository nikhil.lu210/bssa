@extends('layouts.admin.app')

@section('page_title', '| Profile')

@section('stylesheet_links')
{{-- External CSS Links --}}
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" />
<link rel="stylesheet" href="{{ asset('backend/octopus/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
@endsection

@section('stylesheet')
{{--  External CSS  --}}
<style>
.form-bordered .form-group{
    border-bottom: 0px solid #fff;
}

.md-editor.active{
    border-color: #fec62b;
    box-shadow: none !important;
    outline: none !important;
}
.md-editor.active textarea{
    border-top: 1px dashed #fec62b;
}
</style>
@endsection

@section('content')
<header class="page-header">
    {{-- {{dd($profile[0]->name)}} --}}
    <h2><b>{{$profile->name}}</b></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ Route('superadmin.dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Profile</span></li>
            <li><span>{{$profile->name}}</span></li>
        </ol>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <div class="row">
        <div class="col-md-12">
            <header class="panel-heading">
                <div class="panel-actions">
                    <button class="btn btn-default btn-sm" onclick="window.history.go(-1); return false;">Back</button>
                </div>
                <h2 class="panel-title">{{$profile->name}}</h2>
            </header>

            <form class="form-horizontal form-bordered" action="{{ route('admin.profile.update', ['id' => $profile->id]) }}" method="POST">
                @csrf
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="name">Full Name *</label>
                        <div class="col-md-8">
                            <input value="{{$profile->name}}" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="username">Username *</label>
                        <div class="col-md-8">
                            <input value="{{$profile->username}}" type="text" class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}" id="username" name="username">
                            @if ($errors->has('username'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="email">Email *</label>
                        <div class="col-md-8">
                            <input value="{{$profile->email}}" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="mobile">Mobile Number *</label>
                        <div class="col-md-8">
                            <input value="{{$profile->mobile}}" type="phone" class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" id="mobile" name="mobile">
                            @if ($errors->has('mobile'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="note">Note </label>
                        <div class="col-md-8">
                            <textarea class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" name="note" rows="2">{{$profile->note}}</textarea>
                            {{-- <small>If any note about this Admin.</small> --}}
                            @if ($errors->has('note'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('note') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                </div>

                <div class="panel-footer text-right">
                    <button type="submit" class="mb-xs mt-xs mr-xs btn btn-warning">Update Information</button>
                </div>

            </form>
        </div>
    </div>
</section>


{{-- ====================< Change Password >=================== --}}
<section class="panel">
    <div class="row">
        <div class="col-md-12">
            <header class="panel-heading">
                <div class="panel-actions">
                    <button class="btn btn-default btn-sm" onclick="window.history.go(-1); return false;">Back</button>
                </div>
                <h2 class="panel-title">Change Password</h2>
            </header>

            <form class="form-horizontal form-bordered" action="{{ route('admin.profile.updatePassword', ['id' => $profile->id]) }}" method="POST">
                @csrf
                <div class="panel-body">
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        @endif


                    <div class="form-group">
                        <label class="col-md-2 control-label" for="old_password">Old Password *</label>
                        <div class="col-md-8">
                            <input value="{{old('old_password')}}" type="password" class="form-control {{ $errors->has('old_password') ? ' is-invalid' : '' }}" id="old_password" name="old_password">
                            @if ($errors->has('old_password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('old_password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="new_password">New Password *</label>
                        <div class="col-md-8">
                            <input value="{{old('new_password')}}" type="password" class="form-control {{ $errors->has('new_password') ? ' is-invalid' : '' }}" id="new_password" name="new_password">
                            @if ($errors->has('new_password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('new_password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="confirm_password">Confirm Password *</label>
                        <div class="col-md-8">
                            <input value="{{old('confirm_password')}}" type="password" class="form-control {{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" id="confirm_password" name="confirm_password">
                            @if ($errors->has('confirm_password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('confirm_password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                </div>

                <div class="panel-footer text-right">
                    <button type="submit" class="mb-xs mt-xs mr-xs btn btn-warning">Change Password</button>
                </div>

            </form>
        </div>
    </div>
</section>
<!-- end: page -->
@endsection


@section('scripts')
{{--  External Javascript  --}}
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/to-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}"></script>
<script src="{{ asset('backend/octopus/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

@endsection
