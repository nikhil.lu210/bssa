<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id'       =>      '1',
            'name'          =>      'Super Admin',
            'username'      =>      'super_admin',
            'email'         =>      'super_admin@mail.com',
            'mobile'         =>      '01712345679',
            'password'      =>      bcrypt('12345678'),
            'note'          =>      'I am a Super Admin',
            'created_at'    =>      now(),
            'updated_at'    =>      now(),
        ]);

        DB::table('users')->insert([
            'role_id'       =>      '2',
            'name'          =>      'Admin',
            'username'      =>      'admin',
            'email'         =>      'admin@mail.com',
            'mobile'         =>      '01712345678',
            'password'      =>      bcrypt('12345678'),
            'note'          =>      'I am a Admin',
            'created_at'    =>      now(),
            'updated_at'    =>      now(),
        ]);

    }
}
