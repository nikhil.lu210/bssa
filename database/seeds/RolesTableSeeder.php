<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            // 'role_id' => '1',
            'name' => 'Super_Admin',
            'slug' => 'super_admin',
        ]);

        DB::table('roles')->insert([
            // 'role_id' => '2',
            'name' => 'Admin',
            'slug' => 'admin',
        ]);

    }
}
