<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');

            // Relation With Users Table
            $table->unsignedBigInteger('admin_id');
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('name', 191);
            $table->string('email', 50)->nullable();
            $table->string('mobile', 30)->nullable();
            $table->text('address');
            $table->string('post_code', 10);
            $table->date('birthdate');
            $table->boolean('reg_status');
            $table->string('reg_club_name', 191)->nullable();

            $table->string('guardian_name', 191);
            $table->string('relation', 100);
            $table->string('guardian_mobile', 30);
            $table->string('guardian_telephone', 30)->nullable();
            $table->string('guardian_email', 50)->nullable();

            $table->string('emergency_name_one', 191)->nullable();
            $table->string('emergency_mobile_one', 30)->nullable();

            $table->string('emergency_name_two', 191)->nullable();
            $table->string('emergency_mobile_two', 30)->nullable();

            $table->text('medical_note_one')->nullable();
            $table->text('medical_note_two')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');

        Schema::table("members", function ($table) {
            $table->dropSoftDeletes();
        });

    }
}
