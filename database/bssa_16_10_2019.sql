CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);



INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Super_Admin', 'super_admin', NULL, NULL, NULL),
	(2, 'Admin', 'admin', NULL, NULL, NULL);



CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL DEFAULT 2,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
);



INSERT INTO `users` (`id`, `role_id`, `name`, `username`, `email`, `mobile`, `email_verified_at`, `password`, `note`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Super Admin', 'super_admin', 'super_admin@mail.com', '01712345679', NULL, '$2y$10$WNfXFfAA5usgd1Dp/z0Ay.pju8AhdynuKkDn11a2Atd83Y62qAcyS', 'I am a Super Admin', NULL, '2019-10-16 05:49:49', '2019-10-16 05:49:49', NULL),
	(2, 2, 'Admin', 'admin', 'admin@mail.com', '01712345678', NULL, '$2y$10$HITfjlVpTzbNtbPnNEi.0eaDflJ2G.kuMVlKEsmP32gqRy0E9k5du', 'I am a Admin', NULL, '2019-10-16 05:49:49', '2019-10-16 05:49:49', NULL);


CREATE TABLE IF NOT EXISTS `members` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `reg_status` tinyint(1) NOT NULL,
  `reg_club_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guardian_mobile` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guardian_telephone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_name_one` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_mobile_one` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_name_two` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_mobile_two` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medical_note_one` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medical_note_two` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `members_admin_id_foreign` (`admin_id`),
  CONSTRAINT `members_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
);




CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
);




CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);



INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_roles_table', 1),
	(2, '2014_10_12_000001_create_users_table', 1),
	(3, '2014_10_12_100000_create_password_resets_table', 1),
	(4, '2019_07_07_074337_create_members_table', 1);




