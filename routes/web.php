<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::post('/loginwithuser', 'Auth\LoginController@loginWithEmailOrUsername')->name('loginWithUser');


Route::get('/', 'HomeController@index')->name('home');



/*===================================
========< Super Admin Routes >=======
===================================*/
Route::group([
        'middleware' => ['auth', 'superadmin']
    ], function () {
        // Dashboard
        include_once 'super_admin/dashboard.php';

        // Records
        include_once 'super_admin/admin.php';
        include_once 'super_admin/member.php';

        // Profile
        include_once 'super_admin/profile.php';
    }
);



/*===================================
===========< Admin Routes >==========
===================================*/
Route::group([
        'middleware' => ['auth', 'admin']
    ], function () {
        // Dashboard
        include_once 'admin/dashboard.php';

        // Members
        include_once 'admin/member.php';

        // Profile
        include_once 'admin/profile.php';
    }
);
