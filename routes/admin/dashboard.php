<?php

// Admin Routes
Route::group([
    'prefix' => 'admin', //Url
    'namespace' => 'Admin', //Controller
    'as' => 'admin.' //Route
],
    function(){
        Route::get('', 'Dashboard\DashboardController@index')->name('dashboard');
    }
);
