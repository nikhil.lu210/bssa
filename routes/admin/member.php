<?php

// Admin Routes
Route::group([
    'prefix' => 'admin', //Url
    'namespace' => 'Admin', //Controller
    'as' => 'admin.' //Route
],
    function(){
        Route::get('member', 'Member\MemberController@index')->name('member.index');
        Route::get('member/filter', 'Member\MemberController@filter')->name('member.filter');
        Route::get('setting/member/create', 'Member\MemberController@create')->name('member.create');
        Route::post('member/store', 'Member\MemberController@store')->name('member.store');
        Route::get('member/show/{id}', 'Member\MemberController@show')->name('member.show');
        Route::post('member/update/{id}', 'Member\MemberController@update')->name('member.update');
        Route::get('member/destroy/{id}', 'Member\MemberController@destroy')->name('member.destroy');
    }
);
