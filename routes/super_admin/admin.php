<?php

// Super Admin Routes
Route::group([
    'prefix' => 'superadmin', //Url
    'namespace' => 'SuperAdmin\Record', //Controller
    'as' => 'superadmin.record.' //Route
],
    function(){
        Route::get('record/admin', 'Admin\AdminController@index')->name('admin.index');
        Route::get('setting/admin/create', 'Admin\AdminController@create')->name('admin.create');
        Route::post('record/admin/store', 'Admin\AdminController@store')->name('admin.store');
        Route::get('record/admin/show/{id}', 'Admin\AdminController@show')->name('admin.show');
        Route::post('record/admin/update/{id}', 'Admin\AdminController@update')->name('admin.update');
        Route::get('record/admin/destroy/{id}', 'Admin\AdminController@destroy')->name('admin.destroy');
    }
);
