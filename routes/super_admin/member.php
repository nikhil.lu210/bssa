<?php

// Super Admin Routes
Route::group([
    'prefix' => 'superadmin/record', //Url
    'namespace' => 'SuperAdmin\Record', //Controller
    'as' => 'superadmin.record.' //Route
],
    function(){
        Route::get('member', 'Member\MemberController@index')->name('member.index');
        Route::get('member/show/{id}', 'Member\MemberController@show')->name('member.show');
    }
);
