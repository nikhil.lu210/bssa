<?php

// Super Admin Routes
Route::group([
    'prefix' => 'superadmin', //Url
    'namespace' => 'SuperAdmin', //Controller
    'as' => 'superadmin.' //Route
],
    function(){
        Route::get('profile', 'Profile\ProfileController@index')->name('profile.index');
        Route::post('profile/{id}', 'Profile\ProfileController@update')->name('profile.update');
        Route::post('profile/updatePassword/{id}', 'Profile\ProfileController@updatePassword')->name('profile.updatePassword');
    }
);
