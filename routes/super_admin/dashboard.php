<?php

// Super Admin Routes
Route::group([
    'prefix' => 'superadmin', //Url
    'namespace' => 'SuperAdmin', //Controller
    'as' => 'superadmin.' //Route
],
    function(){
        Route::get('', 'Dashboard\DashboardController@index')->name('dashboard');
    }
);
