$(document).ready(function() {
    $(".sidebar-toggle.lg").on("click", function() {
        $(".sidebar-toggle.lg").addClass("d-none");
        $(".sidebar-toggle.sm").removeClass("d-none");
    });

    $(".sidebar-toggle.sm").on("click", function() {
        $(".sidebar-toggle.sm").addClass("d-none");
        $(".sidebar-toggle.lg").removeClass("d-none");
    });
});
