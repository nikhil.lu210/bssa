<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(!Auth::check()) return view('welcome');
        if (Auth::check() && Auth::user()->role->id == 1) {
            return redirect()->route('superadmin.dashboard');
        } elseif (Auth::check() && Auth::user()->role->id == 2) {
            return redirect()->route('admin.dashboard');
        } else{
            return $next($request);
        }
    }
}
