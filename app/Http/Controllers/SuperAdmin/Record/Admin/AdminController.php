<?php

namespace App\Http\Controllers\SuperAdmin\Record\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = User::where('role_id', 2)->get();

        return view('super_admin.record.admin.index')->withAdmins($admins);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('super_admin.record.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'              =>  'required | max:191 | string',
            'username'          =>  'required | string | max:20 | unique:users',
            'email'             =>  'required | email | unique:users',
            'mobile'            =>  'required | max:20 | string',
            'password'          =>  'required | min:8 | max:30 | string',
        ]);

        if($request->note){
            $this->validate($request, [
                'note'     =>  'string',
            ]);
        }

        $admin = new User();

        $admin->name        = $request->name;
        $admin->username    = $request->username;
        $admin->email       = $request->email;
        $admin->mobile      = $request->mobile;

        if($request->note)
            $admin->note        = $request->note;

        $admin->password    = bcrypt($request->password);

        $admin->save();

        return redirect()->route('superadmin.record.admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = User::find($id);
        return view('super_admin.record.admin.show')->withAdmin($admin);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = User::find($id);


        $this->validate($request, [
            'name'              =>  'required | max:191 | string',
            'mobile'            =>  'required | max:20 | string',
        ]);

        if($admin->username != $request->username){
            $this->validate($request, [
                'username'          =>  'required | string | max:20 | unique:users',
            ]);
        }
        if($admin->email != $request->email){
            $this->validate($request, [
                'email'             =>  'required | email | unique:users',
            ]);
        }

        if($request->note){
            $this->validate($request, [
                'note'     =>  'string',
            ]);
        }

        if($request->passowrd){
            $this->validate($request, [
                'password'          =>  'required | min:8 | max:30 | string',
            ]);
        }


        if($request->name)
            $admin->name        = $request->name;

        if($request->username)
            $admin->username    = $request->username;

        if($request->email)
            $admin->email       = $request->email;

        if($request->mobile)
            $admin->mobile      = $request->mobile;

        if($request->note)
            $admin->note        = $request->note;

        if($request->password)
            $admin->password    = bcrypt($request->password);

        $admin->save();

        return redirect()->route('superadmin.record.admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = User::find($id);
        // dd($admin);
        $admin->delete();

        return redirect()->back();
    }
}
