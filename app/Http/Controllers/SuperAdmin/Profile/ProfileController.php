<?php

namespace App\Http\Controllers\SuperAdmin\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

Use App\User;
use Auth;
use Validator;
use Hash;
use Session;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = User::where('id', Auth::user()->id)->first();
        // dd($profile);
        return view('super_admin.profile.index')->withProfile($profile);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = User::find($id);

        $this->validate($request, [
            'name'              =>  'required | max:191 | string',
            'mobile'            =>  'required | max:20 | string',
        ]);

        if($profile->username != $request->username){
            $this->validate($request, [
                'username'          =>  'required | string | max:20 | unique:users',
            ]);
        }
        if($profile->email != $request->email){
            $this->validate($request, [
                'email'             =>  'required | email | unique:users',
            ]);
        }

        if($request->note){
            $this->validate($request, [
                'note'     =>  'string',
            ]);
        }


        $profile->name        = $request->name;
        $profile->username    = $request->username;
        $profile->email       = $request->email;
        $profile->mobile      = $request->mobile;
        $profile->note        = $request->note;

        $profile->save();

        return redirect()->back();
    }

    /**
     * Update the specified password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function updatePassword(Request $request, $id)
    // {
    //     $profile = User::find($id);

    //     $this->validate($request, [
    //         'old_password'          =>      'required',
    //         'new_password'          =>      'required | string | min:8 ',
    //         'confirm_password'      =>      'required | string | min:8 | confirmed',
    //     ]);
    //     dd($request);
    // }


    public function admin_credential_rules(array $data)
    {
        $messages = [
            'old_password.required' => 'Please enter current password',
            'newPassword.required' => 'Please enter password',
        ];

        $validator = Validator::make($data, [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password',
        ], $messages);

        return $validator;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function updatePassword(Request $request){

        if(Auth::Check()){
            $request_data = $request->All();
            $validator = $this->admin_credential_rules($request_data);
            if($validator->fails()){
                // return response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);
                Session::flash('error', 'password did not match, Please try again!!!');
                return redirect()->back();
            }
            else{
                $current_password = Auth::User()->password;
                if(Hash::check($request_data['old_password'], $current_password)){
                    $user_id = Auth::User()->id;
                    $obj_user = User::find($user_id);
                    $obj_user->password = Hash::make($request_data['new_password']);
                    $obj_user->save();
                    Session::flash('success', 'password successfully changed!!!');
                    return redirect()->back();
                }
                else{
                    return redirect()->back()->with("error", "Please enter correct current password");
                    // $error = array('old_password' => 'Please enter correct current password');
                    // return response()->json(array('error' => $error), 400);
                }
            }
        }
        else{
            return redirect()->back();
        }

     }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
