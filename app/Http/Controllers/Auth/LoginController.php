<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (Auth::check() && Auth::user()->role->id == 1) {
            $this->redirectTo = route('superadmin.dashboard');
        }
        else{
            $this->redirectTo = route('admin.dashboard');
        }

        $this->middleware('guest')->except('logout');
    }

    // After login where to go
    public function redirectAuth()
    {
        if (Auth::check() && Auth::user()->role->id == 1) {
            $redirectTo = route('superadmin.dashboard');
        }
        else{
            $redirectTo = route('admin.dashboard');
        }

        $this->middleware('guest')->except('logout');
        // dd($redirectTo);
        return $redirectTo;
    }


    // Login with username or email
    public function loginWithEmailOrUsername(Request $request){

        $this->validate($request, [
            'email'         => 'required',
            'password'      => 'required | min:8 | string',
        ]);
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            return redirect()->to($this->redirectAuth());
                // return "logged in email";
        } elseif(Auth::attempt(['username' => $request->email, 'password' => $request->password], $request->remember)){
            return redirect()->to($this->redirectAuth());
            // return "logged in by username";
        } else {
            return redirect()->to($this->redirectAuth());
            // return "not logged in";
        }
    }


}
