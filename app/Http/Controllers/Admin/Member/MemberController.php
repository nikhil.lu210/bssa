<?php

namespace App\Http\Controllers\Admin\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Model\Member;
use Auth;
use DateTime;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::all();
        return view('admin.member.index')->withMembers($members);
    }


    public function filter()
    {
        // $members = Member::all();
        $members = User::with('members')->where('id', Auth::user()->id)->first();
        $members = $members->members;
        return view('admin.member.filter')->withMembers($members);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'              =>  'required | max:191 | string',
            'address'           =>  'required | string',
            'post_code'         =>  'required | max:10 | string',
            'birthdate'         =>  'required | date | date_format:m/d/Y',
            'reg_status'        =>  'required',
            'guardian_name'     =>  'required | max:191 | string',
            'relation'          =>  'required | max:191 | string',
            'guardian_mobile'   =>  'required | max:191 | string',
        ]);

        if($request->reg_status == 1){
            $this->validate($request, [
                'reg_club_name'     =>  'required | max:191 | string',
            ]);
        }

        if(isset($request->email)){
            $this->validate($request, [
                'email' => 'email',
            ]);
        }

        if(isset($request->mobile)){
            $this->validate($request, [
                'mobile' => 'string | max:30',
            ]);
        }

        if(isset($request->guardian_telephone)){
            $this->validate($request, [
                'guardian_telephone' => 'string | max:30',
            ]);
        }

        if(isset($request->guardian_email)){
            $this->validate($request, [
                'guardian_email' => 'email',
            ]);
        }

        if(isset($request->emergency_name_one)){
            $this->validate($request, [
                'emergency_name_one' => 'string | max:191',
            ]);
        }

        if(isset($request->emergency_mobile_one)){
            $this->validate($request, [
                'emergency_mobile_one' => 'string | max:30',
            ]);
        }

        if(isset($request->emergency_name_two)){
            $this->validate($request, [
                'emergency_name_two' => 'string | max:191',
            ]);
        }

        if(isset($request->emergency_mobile_two)){
            $this->validate($request, [
                'emergency_mobile_two' => 'string | max:30',
            ]);
        }

        if(isset($request->medical_note_one)){
            $this->validate($request, [
                'medical_note_one' => 'string',
            ]);
        }

        if(isset($request->medical_note_two)){
            $this->validate($request, [
                'medical_note_two' => 'string',
            ]);
        }
        // dd($request);


        $member = new Member();

        $member->admin_id               =      Auth::user()->id;
        $member->name                   =      $request->name;
        $member->email                  =      $request->email;
        $member->mobile                 =      $request->mobile;
        $member->address                =      $request->address;
        $member->post_code              =      $request->post_code;
        $member->birthdate              =      $this->dateFormat($request->birthdate);
        $member->reg_status             =      $request->reg_status;
        $member->reg_club_name          =      $request->reg_club_name;
        $member->guardian_name          =      $request->guardian_name;
        $member->relation               =      $request->relation;
        $member->guardian_mobile        =      $request->guardian_mobile;
        $member->guardian_telephone     =      $request->guardian_telephone;
        $member->guardian_email         =      $request->guardian_email;
        $member->emergency_name_one     =      $request->emergency_name_one;
        $member->emergency_mobile_one   =      $request->emergency_mobile_one;
        $member->emergency_name_two     =      $request->emergency_name_two;
        $member->emergency_mobile_two   =      $request->emergency_mobile_two;
        $member->medical_note_one       =      $request->medical_note_one;
        $member->medical_note_two       =      $request->medical_note_two;

        // dd($member->birthdate);
        $member->save();

        return redirect()->route('admin.member.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::find($id);
        return view('admin.member.show')->withMember($member);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'              =>  'required | max:191 | string',
            'address'           =>  'required | string',
            'post_code'         =>  'required | max:10 | string',
            'birthdate'         =>  'required | date | date_format:m/d/Y',
            'reg_status'        =>  'required',
            'guardian_name'     =>  'required | max:191 | string',
            'relation'          =>  'required | max:191 | string',
            'guardian_mobile'   =>  'required | max:191 | string',
        ]);

        if($request->reg_status == 1){
            $this->validate($request, [
                'reg_club_name'     =>  'required | max:191 | string',
            ]);
        }

        if(isset($request->email)){
            $this->validate($request, [
                'email' => 'email',
            ]);
        }

        if(isset($request->mobile)){
            $this->validate($request, [
                'mobile' => 'string | max:30',
            ]);
        }

        if(isset($request->guardian_telephone)){
            $this->validate($request, [
                'guardian_telephone' => 'string | max:30',
            ]);
        }

        if(isset($request->guardian_email)){
            $this->validate($request, [
                'guardian_email' => 'email',
            ]);
        }

        if(isset($request->emergency_name_one)){
            $this->validate($request, [
                'emergency_name_one' => 'string | max:191',
            ]);
        }

        if(isset($request->emergency_mobile_one)){
            $this->validate($request, [
                'emergency_mobile_one' => 'string | max:30',
            ]);
        }

        if(isset($request->emergency_name_two)){
            $this->validate($request, [
                'emergency_name_two' => 'string | max:191',
            ]);
        }

        if(isset($request->emergency_mobile_two)){
            $this->validate($request, [
                'emergency_mobile_two' => 'string | max:30',
            ]);
        }

        if(isset($request->medical_note_one)){
            $this->validate($request, [
                'medical_note_one' => 'string',
            ]);
        }

        if(isset($request->medical_note_two)){
            $this->validate($request, [
                'medical_note_two' => 'string',
            ]);
        }
        // dd($request);


        $member = Member::find($id);

        $member->admin_id               =      Auth::user()->id;
        $member->name                   =      $request->name;
        $member->email                  =      $request->email;
        $member->mobile                 =      $request->mobile;
        $member->address                =      $request->address;
        $member->post_code              =      $request->post_code;
        $member->birthdate              =      $this->dateFormat($request->birthdate);
        $member->reg_status             =      $request->reg_status;
        $member->reg_club_name          =      $request->reg_club_name;
        $member->guardian_name          =      $request->guardian_name;
        $member->relation               =      $request->relation;
        $member->guardian_mobile        =      $request->guardian_mobile;
        $member->guardian_telephone     =      $request->guardian_telephone;
        $member->guardian_email         =      $request->guardian_email;
        $member->emergency_name_one     =      $request->emergency_name_one;
        $member->emergency_mobile_one   =      $request->emergency_mobile_one;
        $member->emergency_name_two     =      $request->emergency_name_two;
        $member->emergency_mobile_two   =      $request->emergency_mobile_two;
        $member->medical_note_one       =      $request->medical_note_one;
        $member->medical_note_two       =      $request->medical_note_two;

        $member->save();

        return redirect()->route('admin.member.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::find($id);

        $member->delete();

        return redirect()->back();
    }



    protected function dateFormat($date){
        $formated = explode("/",$date);
        return $formated[2]. "-" .$formated[0]."-".$formated[1];
    }

}
