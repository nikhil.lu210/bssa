<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    public function user(){
        return $this->belongsTo('App\User', 'admin_id', 'id');
    }
    protected $dates = ['deleted_at'];
}
